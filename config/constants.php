<?php

//define's all the constants being used by the app

return [

    "ENQUIRY_MADE_FOR" => ['myself', 'friend', 'daughter', 'son', 'wife', 'husband', 'relative', 'others'],
    "ENQUIRY_STATUS" => ['1' => 'Follow Up', '2' => 'Admission', '3' => 'Rejected']
];
