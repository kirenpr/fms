<?php

//define's all the messages being used by the app

return [

    "GLOBAL_UPDATE_SUCCESS" => "Changes have been saved.",
    "GLOBAL_ERROR" => "Sorry, an error occurred while processing your request. Please try again later.",
    "GLOBAL_CREATE_SUCCESS" => "{entity} has been created successfully.",
    "GLOBAL_DELETE" => "Record deleted successfully"
];
