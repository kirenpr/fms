<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::create([
           "name" => "Kiren Chandh P R",
           "email" => "kirenchandh@gmail.com",
           "password" => Hash::make('kiren@123'),
           "remember_token" => "asnjjasdio!@!"
       ]); 
    }
}
