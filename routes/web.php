<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('login');
});

//Login page view
Route::view("login", "login");

//Login action
Route::post("login", "LoginController@login")->name('login');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('reset-password');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('reset-password-action');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset-form');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('reset-form-action');

//Logout action
Route::post("logout", "LoginController@logout")->name('logout');

//Manage user based dashboard
Route::get('/dashboard', 'DashboardController@view')->name('dashboard');

//routes for enquiries
Route::get("enquiries/view", "EnquiryController@view")->name("enquiries-view");
Route::get("enquiries/showAddForm", "EnquiryController@showAddForm")->name("enquiries-add-form");
Route::post("enquiries/store", "EnquiryController@store")->name("enquiries-store");
Route::get("enquiries/showEditForm/{id}", "EnquiryController@showEditForm")->name("enquiries-edit-form");
Route::post("enquiries/update/{id}", "EnquiryController@update")->name("enquiries-update");
Route::get("enquiries/delete/{id}", "EnquiryController@delete")->name("enquiries-delete");
Route::post("enquiries/forward/{id}", "EnquiryController@forward")->name("enquiries-forward");
Route::post("enquiries/enquiries-change-status/{id}", "EnquiryController@changeStatus")->name("enquiries-change-status");

//routes for follow ups
Route::get("followup/view", "FollowupController@view")->name("followup-view");



