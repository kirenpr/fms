/* 
 * Initializes all the plugins being used by the app
 */
$(document).ready(function () {

    //Select2 plugin
    initSelect2('.select2');

    //datepicker with disabled past dates
    if ($('.datepicker-past-disabled').length > 0) {
        $('.datepicker-past-disabled').datepicker({
            startDate: '0d',
            autoclose: true,
            todayHighlight: true
        });
    }

    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    }

    /* 
     *  toastr 
     */
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
    };

//Datatable init
    if ($(".datatable").length > 0) {

        initDatatable(".datatable");
    }
});