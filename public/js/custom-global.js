
/**
 * Javascript file for - All global event handlers and variables declarations
 */

/*
 * Init Global Messages
 */
//Success Message template - create action
globalSuccessMsg = "{entity} has been created successfully";
//Registration Success message
globalRegSuccessMsg = 'Congratulation! you have successfully registered a new staff.';
//Message to be shown by default Globally for all successfully updation of an entity
globalUpdateSuccessMsg = "Changes have been saved.";
//Global delete success message
globalDeleteSuccessMsg = "Record deleted successfully";
//Error message
globalErrorMsg = "Sorry, an error occurred while processing your request. Please try again later.";
//Message for invalid form submission data while using AJAX
globalInvalidFormData = "The submitted data is invalid. Please fix the error's and submit again!";
//No data found msg
globalNoDataMsg = 'No data found.';
//global confirmation message for delete action
globalDeleteConfirmMsg = 'Are you sure you want to delete the selected record?';
//global confirmation message for update action
globalUpdateConfirmMsg = 'Are you sure you want to save this change?';
//Initialize datatables
DataTable = {};

$(document).ready(function () {

    /**
     * set up for automatically adding csrf token to all ajax requests headers
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     *Add data attribute 'hasloader' to all buttons and enable it on click,
     *so that it can be used to show button loaders.
     */
    $("body").on("click", "button", function () {

        $(this).addClass("btn-loader-js");
    });

    /*
     * allow only numbers to enter
     */

    $("body").on('keyup focusout', ".allowNumbersOnly", function () {

        return allowNumbersrOnly(this);
    });


});