/**
 * Custom js file for global functions used by the app
 */


/**
 * Allows only numbers.
 * 
 * @param {string|object} ele DOM element
 * @returns {Boolean}
 */

function allowNumbersrOnly(ele) {

    $(ele).val($(ele).val().replace(/\D/g, ''));
    return true;
}


/**
 * The Global Ajax Handler Method. Handles all the ajax request made by the application.
 * 
 * @param {string} url A string containing the URL to which the request is sent.
 * @param {string} method The HTTP method to use for the request (e.g. "POST", "GET", "PUT"). Default is 'GET'
 * @param {PlainObject, String or Array} data Data to be sent to the server.
 * @param {callback} successCallback The callback function to be called on success.
 * @param {PlainObject} options other options to be used by the handler
 * @returns {Boolean} true on success. false on any failure.
 */

function globalAjaxHandler(url, method, data, successCallback, options) {

    if (url === '') {

        return false;
    }

    showButtonLoader();

    $.ajax({
        url: url,
        method: method === '' ? 'GET' : method,
        data: data === '' || data.length === 0 ? {} : data,
        processData: options.processData,
        contentType: options.contentType,
        error: function (xhr, status, error) {

            removeButtonLoader();
            error === "Unprocessable Entity" ? showToastr('error', globalInvalidFormData) : showToastr('error', globalErrorMsg);
            return false;
        },
        statusCode: {
            422: function (response) {

                inValidFormDataErrorHandler(response.responseJSON)
            }
        },
        success: function (response) {

            removeButtonLoader();

            if (successCallback) {

                successCallback(response);
            }
            return true;
        }
    });

    return true;
}


/**
 * 
 * @param {type} title
 * @param {type} message
 * @param {type} url
 * @param {type} method
 * @param {type} data
 * @param {type} successCallback
 * @param {Object} options
 * @returns {Boolean}
 * 
 */
function globalConfirmHandler(title, message, url, method, data, successCallback, options) {

    if (title === '') {

        return false;
    }

    $.confirm({
        title: "Confirm " + title,
        content: message === '' ? globalDeleteConfirmMsg : message,
        type: 'green',
        buttons: {
            formSubmit: {
                text: 'Yes',
                btnClass: 'btn-green',
                action: function () {

                    if (options.formSubmitAction) {
                        
                        return options.formSubmitAction();
                    }

                    globalAjaxHandler(url, method, data, successCallback, options);

                    return true;
                }
            },
            no: {
                text: 'No',
                btnClass: 'btn-red',
                keys: ['esc'],
                action: options.actionForNo
            }
        }
    });
}

/**
 * 
 * @param {type} response
 * @returns {undefined}
 */
function inValidFormDataErrorHandler(response) {

    if (response.errors) {

        $.each(response.errors, function (id, error) {

            var domEle = $("body").find("#" + id);
            if (domEle.length > 0) {

                domEle.parent().find(".help-block").html('<ul class="list-unstyled"><li>' + error + '</li></ul>');
                domEle.parent().addClass("has-error has-danger");
            }
        });
    }
}

/**
 * 
 * @param {type} ele
 * @returns {undefined}
 */
function initSelect2(ele) {

    ele = ele === '' ? ".select2" : ele;

    if ($(ele).length > 0) {
        $(ele).select2({
            placeholder: "Please Select"
        });
    }
}

function showToastr(type, msg) {

    toastr[type](msg);
}

function removeToastr() {

    toastr.remove();
}

function showButtonLoader() {

    var ele = $("body").find(".btn-loader-js");

    if (ele.length === 0) {

        return false;
    }
    var loaderLabel = ele.data("loaderlabel") ? ele.data("loaderlabel") : "please wait...";

    ele.html('<i class="fa fa-refresh fa-spin"></i> ' + loaderLabel);
}

function removeButtonLoader() {

    var ele = $("body").find(".btn-loader-js");

    if (ele.length === 0) {

        return false;
    }
    var btnLabel = ele.data("btnlabel") ? ele.data("btnlabel") : "Submit";

    ele.html(btnLabel);
    ele.removeClass("btn-loader-js");
}

/**
 * clear form
 * @returns {Boolean}
 */
function clearForm() {

    //clear all input visible fields.
    $("form").find('input[type="text"], input[type="number"],select, textarea, input[type="email"], input[type="password"]').each(function () {

        $(this).val('');
    });

    //clear form validation messages
    $("form").find('div.help-block').each(function () {

        $(this).html('');
    });

    $(".radio-clear").find("input[type=radio]:first").prop("checked", true);
    //clear all checkbox's
    $(".checkbox-clear").find("input[type=checkbox]").prop("checked", false);

//clear select2 select box's
    $("form").find(".select2").each(function () {
        var select = this;
        if (!$(select).hasClass('no-clear')) {

            $(select).val(null);
            initSelect2(select);
        }
    });

    //clear all form errors
    $(".help-block").html('');
    $(".form-group").removeClass("has-error has-danger");
    $(".help-block").parent() ? $(".help-block").parent().removeClass("has-error has-danger") : null;

    return true;
}

function initDatatable(ele) {

    distroyDatatable(ele);

    DataTable = $(ele).DataTable({
        "order" : []
    });
}

function distroyDatatable(ele) {

    if ($.fn.DataTable.isDataTable(ele)) {

        $(ele).DataTable().destroy();
    }
    return true;
}