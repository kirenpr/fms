<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'FMS') }}</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <!-- template style -->
        <link rel="stylesheet" href="{{ asset('css/template.min.css') }}">
        <!-- template skin -->
        <link rel="stylesheet" href="{{ asset('css/skins.min.css') }}">
        <!-- custom style -->
        <link rel="stylesheet" href="{{ asset('css/plugins/toastr/toastr.css') }}">
        <!-- custom style -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        @section('child-css')
        @show

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- header section start's-->
            @section('header')
            @include('layouts.header')
            @show

            <!-- header section ends. Sidebar section start's-->
            @section('sidebar')
            @include('layouts.sidebar')
            @show

            <!-- sidebar section ends. Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>

            <!-- header section ends. Sidebar section start's-->
            @section('footer')
            @include('layouts.footer')
            @show

        </div>

        <!-- JQuery Library 2.2.3 -->
        <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Toastr Plugin -->
        <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
        <!-- core custom js written for the app -->
        <script src="{{ asset('js/app.js') }}"></script>
        <!-- custom global javascript -->
        <script src="{{ asset('js/custom-global.js') }}"></script>
        <!-- custom javascript functions -->
        <script src="{{ asset('js/custom-global-functions.js') }}"></script>

        <!-- show messages in session with Toastr -->

        <script>
$(document).ready(function () {

});
        </script>

        @section('child-js')
        @show

        <!-- Plugins Initializer -->
        <script src="{{ asset('js/plugins-init.js') }}"></script>

    </body>
</html>
