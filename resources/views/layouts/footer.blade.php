<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> {{ env('APP_VERSION', 'unknown') }}
    </div>
    <strong>Copyright &copy; {{ @date('Y') }} <a href="#">FMS</a>.</strong> All rights
    reserved.
</footer>