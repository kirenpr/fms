@extends('layouts.app')

@section('child-css')

<!-- select2's -->
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.min.css') }}">

<!-- datepicker -->
<link rel="stylesheet" href="{{ asset('css/plugins/datepicker/datepicker3.css') }}">

@endsection

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Enquiry
        <small>Add</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('enquiries-view') }}">Enquiries</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ route("enquiries-store") }}" data-toggle="validator" method="POST" id="enquiry_form"  novalidate="">
                    @csrf
                    <div class="box-body">
                        <div class='col-xs-6'>
                            <div class="form-group">
                                <label for="name">Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name='name' required id="name" placeholder="Enter Name">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="mobile_number">Mobile Number<span class="mandatory">*</span></label>
                                <input type="text" maxlength="10" class="form-control allowNumbersOnly" required id="mobile_number" name='mobile_number' placeholder="Enter Mobile Number">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="text" data-error="Please enter a valid email address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" class="form-control" id="email" name='email' placeholder="Enter Email address">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address<span class="mandatory">*</span></label>
                                <textarea class="form-control" required="" name='address' rows="3" id="address" placeholder="Enter address..."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="highest_qualification">Highest Qualification<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" required="" id="highest_qualification" name='highest_qualification' placeholder="Highest education">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class='col-xs-6'>
                            <div class="form-group">
                                <label for="courses_interested">Courses Interested<span class="mandatory">*</span></label>
                                <select class="form-control select2" data-validate="false" multiple="multiple" name="courses_interested[]" data-placeholder="Select a course" style="width: 100%;" id='courses_interested'>
                                    @foreach($courses as $eachCourse)
                                    <option value="{{ $eachCourse->id }}">{{ $eachCourse->name }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="remarks">Remarks<span class="mandatory">*</span></label>
                                <textarea class="form-control" id='remarks' required="" name='remarks' rows="3" placeholder="Enter remarks..."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="enquiry_made_for">Enquiry Made For<span class="mandatory">*</span></label>
                                <select class="form-control select2" required id='enquiry_made_for' data-placeholder="Select enquiry made for" name="enquiry_made_for" style="width: 100%;">
                                    <option></option>
                                    @foreach($enquiryMadeFor as $eachEnquiryMadeFor)
                                    <option value='{{ $eachEnquiryMadeFor }}'>{{ $eachEnquiryMadeFor }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <fieldset id="enquiry_made_for_details_js" class="hidden">
                                    <legend>Details</legend>
                                    <div class="col-xs-4">
                                        <input type="text" data-validate="false" class="form-control" name='enquiry_made_for_name' id="enquiry_made_for_name" placeholder="Enter Name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" data-validate="false" maxlength="10" class="form-control allowNumbersOnly" id="enquiry_made_for_mobile_number" name='enquiry_made_for_mobile_number' placeholder="Enter Mobile Number">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <input type="email" class="form-control" id="enquiry_made_for_email" name='enquiry_made_for_email' placeholder="Enter Email address">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <label for="followup-on">Follow up on<span class="mandatory">*</span></label>
                                <input type="text" class="form-control datepicker-past-disabled" required="" id="followup_on" name='followup_on' placeholder="Next followup date">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group radio-clear">
                                <label for="enquiry_status">Enquiry Status</label>
                                @foreach($enquiryStatus as $key => $eachStatus)
                                <div class="radio">

                                    <label><input type="radio" name="enquiry_status" id="enquiry_type" value="{{ $key }}" {{ $key == 1 ? 'checked' : '' }}>{{ $eachStatus }}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" name="submit" value="submitAndAddNext" data-leavepage="0" data-btnlabel="Submit & Add Next" class="btn btn-success submit-btn">Submit & Add Next</button>
                        <button type="submit" name="submit" value="submitAndExit" data-leavepage="1" data-btnlabel="Submit & Exit" class="btn btn-primary submit-btn">Submit & Exit</button>
                        <a href="{{ route('enquiries-view') }}" class="btn btn-default">Cancel</a>
                        <button type="button" id="form_reset_js" class="btn btn-danger">Reset</button>
                    </div>
                    <input type="hidden" value="" id="clicked_submit_btn_identifier_js"/>
                    <input type="hidden" value="{{ route("enquiries-view") }}" id="enquiries_view_route"/>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection

@section('child-js')

<!-- select2 -->
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

<!-- datepicker -->
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<!-- bootstrap validator -->
<script src="{{ asset('js/plugins/bootstrap-validator/validator.js') }}"></script>

<!-- page script -->
<script>
var enquiryAddPageJSActions = function () {

    var enquiryEventHandlers = function () {

        $(".submit-btn").on("click", function () {

            $("#clicked_submit_btn_identifier_js").val($(this).val());
        });

        $("#form_reset_js").on("click", function () {

            clearForm();

            $("#enquiry_made_for_details_js").addClass("hidden");
        });

        $("#enquiry_made_for").on("change", function () {

            var enquiryMadeFor = $(this).val(),
                    detailsEle = $("#enquiry_made_for_details_js");

            if (enquiryMadeFor !== "myself") {

                detailsEle.removeClass("hidden");
            } else {

                detailsEle.addClass("hidden");
            }
        });

        $("#enquiry_form").validator().on('submit', function (e) {

            if (e.isDefaultPrevented()) {

                return false;
            } else {

                e.preventDefault();

                if (customFormValidator() === true) {

                    return false;
                }

                var formEle = $(this);
                var formData = formEle.serialize();
                var url = formEle.attr("action");
                var successCallback = function (jsonResponse) {

                    showToastr(jsonResponse.status, jsonResponse.msg)

                    if ($("#clicked_submit_btn_identifier_js").val() === "submitAndAddNext") {

                        clearForm();
                    } else {

                        window.onbeforeunload = null;
                        setTimeout(function () {

                            window.location = $("#enquiries_view_route").val();
                        }, 1000);
                    }

                    return true;
                };

                return globalAjaxHandler(url, 'POST', formData, successCallback, {});
            }
        });
    };

    function customFormValidator() {

        var hasError = false,
                courseEle = $("#courses_interested"),
                courseEleParent = courseEle.parent(".form-group"),
                enquiryMadeFor = $("#enquiry_made_for"),
                enquiryMadeForDetailsEle = $("#enquiry_made_for_details_js");

        if (courseEle.val() === null) {

            courseEleParent.find(".help-block").html('<ul class="list-unstyled"><li>Please fill out this field.</li></ul>');
            courseEleParent.addClass("has-error has-danger");
            hasError = true;
        } else {

            courseEleParent.find(".help-block").html('');
            courseEleParent.removeClass("has-error has-danger");
        }

        if (enquiryMadeFor.val() === "myself") {

            enquiryMadeForDetailsEle.find("input[type=text]").each(function () {

                $(this).parent().find(".help-block").html('');
            });

        } else {

            enquiryMadeForDetailsEle.find("input[type=text]").each(function () {

                if ($(this).val() === "") {
                    hasError = true;
                    $(this).parent().addClass("has-error has-danger")
                    $(this).parent().find(".help-block").html('<ul class="list-unstyled"><li>Please fill out this field.</li></ul>');
                } else {

                    $(this).parent().removeClass("has-error has-danger");
                    $(this).parent().find(".help-block").html('');
                }
            });
        }

        return hasError;
    }

    return {
        init: function () {

            enquiryEventHandlers();
        }
    };
}();

$(document).ready(function () {

    enquiryAddPageJSActions.init();

    window.onbeforeunload = function () {

        return "Are you sure you want to leave this page?"
    };
});
</script>
@endsection