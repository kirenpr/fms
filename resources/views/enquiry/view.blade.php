@extends('layouts.app')

@section('child-css')

<!-- datatable's -->
<link rel="stylesheet" href="{{ asset('css/plugins/datatables/dataTables.bootstrap.css') }}"/>

<!-- select2's -->
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.min.css') }}"/>

<!-- datepicker -->
<link rel="stylesheet" href="{{ asset('css/plugins/datepicker/datepicker3.css') }}"/>

<!-- jquery-confirm -->
<link rel="stylesheet" href="{{ asset('css/plugins/jquery-confirm/jquery-confirm.min.css') }}"/>

@endsection

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Enquiries
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Enquiries</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-success {{ old("enquirySearch") == "search" ? '' : 'collapsed-box' }}">
                <div class="box-header">
                    <h3 class="box-title">Search Enquiries</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa {{ old("enquirySearch") == "search" ? 'fa-minus' : 'fa-plus' }}"></i></button>
                    </div>
                </div>
                <form role="form" action="" data-toggle="validator" method="GET" id="search_form" novalidate="">
                    <div class="box-body">

                        @csrf
                        <div class="row">
                            <div class="col-xs-2">
                                <input type="text" name='name' value="{{ old("name") }}"  class="form-control" placeholder="Name">
                            </div>
                            <div class="col-xs-2">
                                <input type="text" name='mobile' value="{{ old("mobile") }}" class="form-control" placeholder="Mobile">
                            </div>
                            <div class="col-xs-2">
                                <input type="text" name='email' value="{{ old("email") }}" class="form-control" placeholder="Email">
                            </div>
                            <div class="col-xs-2">
                                <select class="form-control select2"  name="courses_interested" data-placeholder="Select a course" style="width: 100%;" id='courses_interested'>
                                    <option></option>
                                    @foreach($courses as $eachCourse)
                                    <option value="{{ $eachCourse->id }}" {{ old("courses_interested") == $eachCourse->id ? "selected" : ''}}>{{ $eachCourse->name }}</option>
                                    @endforeach
                                </select>
                            </div>
<!--                            <div class="col-xs-2">
                                <input type="text" class="form-control datepicker" value="{{ old("followup_on") }}" id="followup_on" name='followup_on' placeholder="Follow up Date">
                            </div>-->

                            <div class="col-xs-2">
                                <select class="form-control select2"  name="enquiry_status" data-placeholder="Select a status" style="width: 100%;" id='enquiry_status'>
                                    <option></option>
                                    @foreach($enquiryStatus as $key => $eachStatus)
                                    <option value="{{ $key }}" {{ old("enquiry_status") == $key ? "selected" : ''}}>{{ $eachStatus }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name='enquirySearch' value='search' class="btn btn-primary btn-sm">Search</button>
                        <a href="{{ route("enquiries-view") }}" class="btn btn-danger btn-sm">Reset</a>
                    </div>
                </form>
            </div>

            <!-- /.box -->

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Of All Enquiries</h3>
                    <div class="box-tools pull-right">
                        <button onclick="window.location.href ='{{ route('enquiries-add-form') }}'" type="button" class="btn btn-primary btn-sm">Add Enquiry</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="DT-enquiries-list" class="datatable table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Highest Qualification</th>
                                    <th>Courses Interested</th>
                                    <th>Remarks</th>
                                    <!--<th>Follow up Date</th>-->
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($allEnquiries as $eachEnquiry)

                                <tr>
                                    <td>{{ $eachEnquiry->name }}</td>
                                    <td>{{ $eachEnquiry->mobile_number }}</td>
                                    <td>{{ $eachEnquiry->email == '' ? 'NA' : $eachEnquiry->email }}</td>
                                    <td>{{ $eachEnquiry->address }}</td>
                                    <td>{{ $eachEnquiry->highest_qualification }}</td>
                                    <td>
                                        @php
                                        $coursesInterested = '';
                                        @endphp
                                        @foreach($eachEnquiry->course as $eachCourse)
                                        @php
                                        $coursesInterested.= $eachCourse->name . ', ';
                                        @endphp
                                        @endforeach

                                        {{ @rtrim($coursesInterested, ', ') }}
                                    </td>
                                    <td title="{{ $eachEnquiry->remarks }}" data-toggle="tooltip">{{ substr($eachEnquiry->remarks, 0, 50) . '...' }}</td>
                                    <!--<td>{{ @date('D, j S M Y', strtotime($eachEnquiry->followup_on_date)) }}</td>-->
                                    <td>
                                        <form role="form" action="{{ route("enquiries-change-status", $eachEnquiry->id) }}" method="POST" id="enquiry_status_form"  novalidate="">
                                            @csrf
                                            <select class="form-control select2 enquiry_status"  name="enquiry_status" data-placeholder="Select a status" style="width: 100%;" id='enquiry_status'>
                                                <option></option>
                                                @foreach($enquiryStatus as $key => $eachStatus)
                                                <option value="{{ $key }}" {{ $key == $eachEnquiry->status ? 'selected' : '' }}>{{ $eachStatus }}</option>
                                                @endforeach
                                            </select>
                                        </form>
                                    </td>
                                    <td>
                                        <div class="col-md-3 col-sm-4">
                                            <a href="{{ route('enquiries-edit-form', $eachEnquiry->id) }}" id="edit_form"><i class="fa fa-fw fa-edit"></i></a>
                                            <a href="{{ route('enquiries-forward', $eachEnquiry->id) }}" id="enquiry_forward"><i class="fa fa-fw fa-mail-forward"></i></a>
                                            <a href="{{ route('enquiries-delete', $eachEnquiry->id) }}" class="enquiry_delete"><i class="fa fa-fw fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

@endsection

@section('child-js')

<!-- datatables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- select2 -->
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

<!-- datepicker -->
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<!-- jquery-confirm -->
<script src="{{ asset('js/plugins/jquery-confirm/jquery-confirm.min.js') }}"></script>

<!-- page script -->

<script>
                            var enquiryViewJSAction = function () {

                                var enquiryViewEventHandler = function () {

                                    $(".enquiry_status").on("change", function () {

                                        var thisEle = $(this);
                                        var title = "Change Enquiry Status";
                                        var formSubmitAction = function () {

                                            thisEle.parent('#enquiry_status_form').submit();
                                            return true;
                                        };
                                        globalConfirmHandler(title, globalUpdateConfirmMsg, '', '', {}, '', {"formSubmitAction": formSubmitAction});
                                    });

                                    $(".enquiry_delete").on("click", function (e) {

                                        e.preventDefault();

                                        var thisEle = $(this);
                                        var title = "Delete Enquiry";
                                        var formSubmitAction = function () {

                                            window.location.href = thisEle.attr("href");
                                            return true;
                                        };

                                        globalConfirmHandler(title, globalDeleteConfirmMsg, '', '', {}, '', {"formSubmitAction": formSubmitAction});
                                    });
                                }

                                return {
                                    init: function () {

                                        enquiryViewEventHandler();
                                    }
                                };
                            }();
                            $(document).ready(function () {

                                enquiryViewJSAction.init();
                            });
</script>

@endsection