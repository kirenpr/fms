<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseMaster extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'course_master';
    
    /**
     * get all active courses
     * @return type
     */
    public function getAllActiveCourses() {

        return $this->select()->where('is_active', 1)->get();
    }

    /**
     * 
     * @return type
     */
    public function enquiries() {

        return $this->belongsToMany(CourseMaster::class, 'enquiry_courses');
    }

}
