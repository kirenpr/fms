<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\enquiries;

class Followups extends Model
{
    public function enquiries() {
        
        return $this->belongsTo(enquiries::class);
    }
}
