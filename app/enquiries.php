<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CourseMaster;
use App\Followups;

class enquiries extends Model {

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'followup_after_date',
    ];

    /**
     * 
     * @return type
     */
    public function course() {

        return $this->belongsToMany(CourseMaster::class, 'enquiry_courses');
    }

    public function followups() {

        return $this->hasMany(Followups::class);
    }

    public function getEnquiriesById($id) {

        return $this->with('course')->find($id);
    }

}
