<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\enquiries;
use App\CourseMaster;

class FollowupController extends Controller {

    public function view(Request $request) {

        $user = $request->user()->id;

        $courseModel = new CourseMaster();
        $enquiryModel = new enquiries();

        $queryBuilder = $enquiryModel->with(["course", "followups"])
                ->where("created_by", "=", $user);

//        if (null !== $request->input('enquirySearch')) {
//
//            Input::flash();
//            $this->enquirySearch($queryBuilder, $request);
//        } else {
//
//            $request->session()->forget("_old_input");
//        }

        $allFollowups = $queryBuilder->get()->sortBy("followup_on_date");

        $courses = $courseModel->getAllActiveCourses();
        $followupStatus = Config("constants.ENQUIRY_STATUS");

        return view('followup.view', [
            "allFollowups" => $allFollowups,
            "courses" => $courses,
            "followupStatus" => $followupStatus
        ]);
    }

}
