<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseMaster;
use App\enquiries;
use App\Followups;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class EnquiryController extends Controller {

    public function view(Request $request) {

        $courseModel = new CourseMaster();
        $enquiryModel = new enquiries();

        $queryBuilder = $enquiryModel->with('course');

        if (null !== $request->input('enquirySearch')) {

            Input::flash();
            $this->enquirySearch($queryBuilder, $request);
        } else {

            $request->session()->forget("_old_input");
        }

        $allEnquiries = $queryBuilder->get();

        $courses = $courseModel->getAllActiveCourses();
        $enquiryStatus = Config("constants.ENQUIRY_STATUS");

        return view('enquiry.view', [
            "allEnquiries" => $allEnquiries,
            "courses" => $courses,
            "enquiryStatus" => $enquiryStatus
        ]);
    }

    public function showAddForm() {

        $courseModel = new CourseMaster();
        $courses = $courseModel->getAllActiveCourses();

        $enquiryMadeFor = Config("constants.ENQUIRY_MADE_FOR");
        $enquiryStatus = Config("constants.ENQUIRY_STATUS");

        return view('enquiry.add', [
            "courses" => $courses,
            "enquiryMadeFor" => $enquiryMadeFor,
            "enquiryStatus" => $enquiryStatus
        ]);
    }

    public function showEditForm($id) {

        $courseModel = new CourseMaster();
        $enquiryModel = new enquiries();

        $enquiriesCourse = array();

        $courses = $courseModel->getAllActiveCourses();

        $enquiryMadeFor = Config("constants.ENQUIRY_MADE_FOR");
        $enquiryStatus = Config("constants.ENQUIRY_STATUS");

        $enquiriesData = $enquiryModel->getEnquiriesById($id);

        foreach ($enquiriesData->course as $eachCourse) {

            array_push($enquiriesCourse, $eachCourse->id);
        }

        return view('enquiry.edit', [
            "courses" => $courses,
            "enquiryMadeFor" => $enquiryMadeFor,
            "enquiryStatus" => $enquiryStatus,
            "enquiriesData" => $enquiriesData,
            "enquiriesCourse" => $enquiriesCourse
        ]);
    }

    public function store(Request $request) {

        $response = array(
            "status" => "error",
            "msg" => Config("messages.GLOBAL_ERROR")
        );

        $validatedData = $this->validateEnquiryData($request);

        $enquiryModel = new enquiries();

        $enquiryModel->name = $validatedData['name'];
        $enquiryModel->mobile_number = $validatedData['mobile_number'];
        $enquiryModel->email_address = $validatedData['email'];
        $enquiryModel->address = $validatedData['address'];
        $enquiryModel->highest_qualification = $validatedData['highest_qualification'];
        $enquiryModel->highest_qualification = $validatedData['highest_qualification'];
        $enquiryModel->remarks = $validatedData['remarks'];
        $enquiryModel->enquiry_made_for = $validatedData['enquiry_made_for'];
        $enquiryModel->enquiry_made_for_name = $request->request->get('enquiry_made_for_name');
        $enquiryModel->enquiry_made_for_mobile = $validatedData['enquiry_made_for_mobile_number'];
        $enquiryModel->enquiry_made_for_email = $validatedData['enquiry_made_for_email'];
        $enquiryModel->created_by = $request->user()->id;
        $enquiryModel->status = $request->request->get('enquiry_status');

        $enquiryModel->save();

        $insertedEnquiry = $enquiryModel->find($enquiryModel->id);

        $coursesToSave = $validatedData["courses_interested"];

        $insertedEnquiry->course()->attach($coursesToSave);
        
        $insertedEnquiry->enquiries(new Followups(["followup_on_date" => Carbon::parse($validatedData['followup_on'])]));

        $response = array(
            "status" => "success",
            "msg" => str_replace("{entity}", "Enquiry", Config("messages.GLOBAL_CREATE_SUCCESS"))
        );

        return response()->json($response);
    }

    public function update($id, Request $request) {

        $response = array(
            "status" => "error",
            "msg" => Config("messages.GLOBAL_ERROR")
        );
        $enquiriesCourse = array();

        $enquiriesModel = new enquiries();
        $enquiriesData = $enquiriesModel->getEnquiriesById($id);

        foreach ($enquiriesData->course as $eachCourse) {

            array_push($enquiriesCourse, $eachCourse->id);
        }

        $validatedData = $this->validateEnquiryData($request);

        $enquiryModel = $enquiriesModel->find($id);

        $enquiryModel->name = $validatedData['name'];
        $enquiryModel->mobile_number = $validatedData['mobile_number'];
        $enquiryModel->email_address = $validatedData['email'];
        $enquiryModel->address = $validatedData['address'];
        $enquiryModel->highest_qualification = $validatedData['highest_qualification'];
        $enquiryModel->highest_qualification = $validatedData['highest_qualification'];
        $enquiryModel->remarks = $validatedData['remarks'];
        $enquiryModel->enquiry_made_for = $validatedData['enquiry_made_for'];
        $enquiryModel->enquiry_made_for_name = $request->request->get('enquiry_made_for_name');
        $enquiryModel->enquiry_made_for_mobile = $validatedData['enquiry_made_for_mobile_number'];
        $enquiryModel->enquiry_made_for_email = $validatedData['enquiry_made_for_email'];
        $enquiryModel->followup_on_date = Carbon::parse($validatedData['followup_on']);
        $enquiryModel->status = $request->request->get('enquiry_status');

        $courseToDelete = array_diff($enquiriesCourse, $validatedData["courses_interested"]);
        $coursesToSave = array_diff($validatedData["courses_interested"], $enquiriesCourse);

        $enquiryModel->course()->detach($courseToDelete);
        $enquiryModel->course()->attach($coursesToSave);
        
        $enquiryModel->enquiries()->associate(["followup_on_date" => Carbon::parse($validatedData['followup_on'])]);
        
        $enquiryModel->save();
        
        $response = array(
            "status" => "success",
            "msg" => Config("messages.GLOBAL_UPDATE_SUCCESS")
        );

        return response()->json($response);
    }

    public function delete($id) {

        $enquiriesModel = enquiries::find($id);
        
        $enquiriesModel->delete();

        return redirect()->route("enquiries-view")->with("success", Config("messages.GLOBAL_DELETE"));
    }

    public function changeStatus($id, Request $request) {

        $enquiriesModel = enquiries::find($id);

        $enquiriesModel->status = $request->request->get('enquiry_status');

        $enquiriesModel->save();

        return redirect()->route("enquiries-view")->with("success", Config("messages.GLOBAL_UPDATE_SUCCESS"));
    }

    private function validateEnquiryData($request) {

        return $request->validate([
                    "name" => 'required',
                    "mobile_number" => 'required|min:10|numeric',
                    "email" => 'nullable|email',
                    "address" => 'required',
                    "highest_qualification" => 'required',
                    "courses_interested" => 'required',
                    "remarks" => 'required',
                    "enquiry_made_for" => 'required',
                    "enquiry_made_for_email" => 'nullable|email',
                    "enquiry_made_for_mobile_number" => 'nullable|min:10|numeric',
                    "followup_on" => 'required'
        ]);
    }

    private function enquirySearch($queryBuilder, $request) {

        $name = $request->query->get("name");
        $mobile = $request->query->get("mobile");
        $email = $request->query->get("email");
        $coursesInterested = $request->query->get("courses_interested");
        $followupOn = $request->query->get("followup_on");
        $enquiryStatus = $request->query->get("enquiry_status");

        if ($coursesInterested !== null) {

            $queryBuilder = $queryBuilder->whereHas("course", function
                    ($query) use ($coursesInterested) {
                $query->where("course_master_id", $coursesInterested);
            });
        }

        if ($name !== null) {

            $queryBuilder = $queryBuilder->where("name", "LIKE", "%" . $name . "%");
        }
        if ($mobile !== null) {

            $queryBuilder = $queryBuilder->where("mobile_number", "LIKE", "%" . $mobile . "%");
        }
        if ($email !== null) {

            $queryBuilder = $queryBuilder->where("email_address", "LIKE", "%" . $email . "%");
        }

        if ($followupOn !== null) {

            $queryBuilder = $queryBuilder->where("followup_on_date", "=", Carbon::parse($followupOn)->format('Y-m-d'));
        }
        if ($enquiryStatus !== null) {

            $queryBuilder = $queryBuilder->where("status", "=", $enquiryStatus);
        }

        return $queryBuilder;
    }

}
